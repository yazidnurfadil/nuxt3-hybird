// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      title: 'CBE',
      htmlAttrs: {
        lang: 'en'
      },
      meta: [
        { charset: 'utf-8' },
        {
          name: 'viewport',
          content:
            'width=device-width, initial-scale=1, maximum-scale=5, user-scalable=no'
        },
        { hid: 'description', name: 'description', content: '' }
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
      ]
    },
  },
  nitro: {
    preset: 'netlify-builder',
    prerender: {
      routes: ['/me/yazid']
    }
  },
  routeRules: {
    // Static page generated on-demand, revalidates in background
    '/me/**': { swr: 600 },
  },
  experimental: {
    payloadExtraction: false
  },
  typescript: {
    shim: false
  }
})
